<?php

use Faker\Generator as Faker;

$factory->define(App\Entities\Role::class, function (Faker $faker) {
    static $index = 0;
    $roleInfo = [
        /* ['admin', '管理員'],
        ['MFG', '廠務'],
        ['AD', '管理'],
        ['LS', '鐳射'] */
        ['管理員'],
        ['主管級'],
        ['部門級'],
        ['一般員工']
    ];

    return [
        /* 'type' => $roleInfo[$index][0],
        'name' => $roleInfo[$index++][1] */
        'name' => $roleInfo[$index++][0]
    ];
});
