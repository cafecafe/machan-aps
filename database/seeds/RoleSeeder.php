<?php

use Illuminate\Database\Seeder;
use App\Entities\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        collect(range(1, 4))->each(function () {
            factory(Role::class)->create();
        });
    }
}
