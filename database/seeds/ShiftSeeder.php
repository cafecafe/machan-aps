<?php

use Illuminate\Database\Seeder;

class ShiftSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('setup_shifts')->insert([
        	'name' => "正常班",
        	'type'=> "正常",
        	'work_on' => date("H:i",strtotime('8:00')),
        	'work_off' => date("H:i",strtotime('17:20')),
        	'rest_group' => 1,
        ]);
        DB::table('rest_setups')->insert([
        	'start' => date("H:i:s",strtotime('10:00')),
        	'end' => date("H:i:s",strtotime('10:10')),
        	'rest_id' => 1,
        	'remark' => "default",
        	'type' => "休息",
        ]);
        DB::table('rest_setups')->insert([
        	'start' => date("H:i:s",strtotime('12:00')),
        	'end' => date("H:i:s",strtotime('12:40')),
        	'rest_id' => 1,
        	'remark' => "default",
        	'type' => "用餐",  	

        ]);
        DB::table('rest_setups')->insert([
        	'start' => date("H:i:s",strtotime('15:00')),
        	'end' => date("H:i:s",strtotime('15:10')),
        	'rest_id' => 1,
        	'remark' => "default",
        	'type' => "休息",  	

        ]);
        DB::table('rest_groups')->insert([

        	'rest_name' => "default",
        	'work_type' => "正常班",
        	"created_at" => now(),
        ]);
    }
}
