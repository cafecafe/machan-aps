<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimulationManufatureOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulation_manufature_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('mo_id');
            $table->string('item_id');
            $table->string('item_name');
            $table->unsignedInteger('qty');
            $table->string('techroutekey_id');
            $table->string('customer');
            $table->string('customer_name');
            $table->date('online_date');
            $table->string('so_id');
            $table->string('status')->nullable();
            $table->date('complete_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulation_manufature_orders');
    }
}
