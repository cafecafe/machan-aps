<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChildPartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('child_parts', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('top_id');
            $table->unsignedInteger('down_id');
            $table->unsignedInteger('row_no');
            $table->unsignedInteger('row_id');
            $table->string('unit_id');
            $table->double('unit_qty')->nullable();
            $table->double('nuse_qty')->nullable();
            $table->double('base_qty')->nullable();
            $table->string('remark')->nullable();
            $table->string('item_id');
            $table->unsignedInteger('level');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('child_parts');
    }
}
