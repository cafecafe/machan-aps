<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchemeCaseToSimulationInitialSchemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simulation_initial_schemes', function (Blueprint $table) {
            $table->integer('scheme_case');
            ;
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simulation_initial_schemes', function (Blueprint $table) {

            $table->dropColumn('scheme_case');
        });
    }
}
