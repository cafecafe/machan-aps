<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('resources', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('resource_id');
            $table->string('resource_name')->nullable();
            $table->string('resource_type');
            $table->string('device_id')->nullable();
            $table->string('device_name')->nullable();
            $table->string('workcenter_id');
            $table->unsignedInteger('device_qty');
            $table->unsignedInteger('device_multiple');
            $table->unsignedInteger('assign_work');
            $table->float('standard_time');
            $table->float('standard_pre_time');
            $table->unsignedInteger('standard_tct')->nullable();
            $table->unsignedInteger('change_time');
            $table->float('paint_length')->nullable();
            $table->unsignedInteger('hook_qty')->nullable();
            $table->unsignedInteger('change_color_time')->nullable();
            $table->unsignedInteger('standard_change_time')->nullable();
            $table->unsignedInteger('standard_rate')->nullable();
            $table->unsignedInteger('empty_hook_distance')->nullable();
            $table->string('item')->nullable();
            $table->string('item_name')->nullable();
            $table->string('batch_transfer_setting')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('resources');
    }
}
