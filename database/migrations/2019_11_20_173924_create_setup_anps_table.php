<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupAnpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_anps', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bill_no');
            $table->string('cu_material_id');
            $table->string('cu_org_id');
            $table->float('cu_tht');
            $table->float('cu_target_prod_qty');
            $table->float('cu_target_person_qty');
            $table->float('cu_ht');
            $table->float('cu_rw_tht');
            $table->float('cu_std_ct');
            $table->float('cu_adj_tht');
            $table->float('cu_adj_ht');
            $table->float('cu_packing_tht');
            $table->float('cu_adj_packing_tht');
            $table->string('cu_wp');
            $table->float('cu_db_rate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_anps');
    }
}
