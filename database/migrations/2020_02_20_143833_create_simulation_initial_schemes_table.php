<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSimulationInitialSchemesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('simulation_initial_schemes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('scheme_id');
            $table->string('so_id');
            $table->string('mo_id');
            $table->string('item_id');
            $table->string('qty');
            $table->string('resource_id');
            $table->string('aps_id');
            $table->date('cu_ush_date')->nullable();
            $table->datetime('scheme_start')->nullable();
            $table->datetime('scheme_end')->nullable();
            $table->datetime('scheme_recommend_lastest_start')->nullable();
            $table->datetime('scheme_recommend_lastest_end')->nullable();
            $table->datetime('scheme_recommend_early_start')->nullable();
            $table->datetime('scheme_recommend_early_end')->nullable();
            $table->string('scheme_status')->nullable();
            $table->unsignedInteger('batch');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('simulation_initial_schemes');
    }
}
