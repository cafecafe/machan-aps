<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupCoatingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setup_coatings', function (Blueprint $table) {


            $table->increments('id');           //id
            
            $table->string('cu_p_id')->nullable();        //編碼

            $table->string('cu_p_material_id')->nullable();   //子件代碼
            
            $table->string('cu_pc_material_id');  //對應子件代碼

            $table->string('cu_p_zone');         //上料區

            $table->float('cu_p_total_len');    //總長

            $table->integer('cu_p_total_hook')->nullable();   //可作業總勾數

            $table->boolean('cu_is_cover')->nullable();      //圓蓋

            $table->integer('cu_a_hookm_num');       //1勾吊數量

            $table->integer('cu_a_material_h_num')->nullable();   //掛件使用吊勾數
            
            $table->integer('cu_rpm');          //輸送帶刻度

            $table->integer('cu_p_cell_hook_num'); //格數(掛件總母勾數) 

            $table->string('cu_hook_no')->nullable();        //吊具編號
            
            $table->float('cu_cuft')->nullable();           //材積 
            
            $table->integer('cu_p_hook_num')->nullable();      //掛件母勾用數

            $table->float('cu_p_def_rate');       //工作速率

            $table->float('cu_ct');             //CT時間      

            $table->float('cu_change_distance');//換模空勾數

            $table->float('cu_change_time');   //換模時間

            $table->string('cu_org_id');         //組織

            $table->integer('cu_tct');      //作業TCT

            $table->timestamps();

        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setup_coatings');
    }
}
