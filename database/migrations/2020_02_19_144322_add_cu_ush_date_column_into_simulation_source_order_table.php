<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCuUshDateColumnIntoSimulationSourceOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('simulation_source_orders', function (Blueprint $table) {
            $table->date('cu_ush_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('simulation_source_orders', function (Blueprint $table) {
            $table->dropColumn('cu_ush_date');
        });
    }
}
