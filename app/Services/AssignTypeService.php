<?php

namespace App\Services;

use App\Entities\Organization;
use App\Entities\Role;

class AssignTypeService
{
    public function role()
    {
        return Role::get();
    }

    public function organization()
    {
        return Organization::get();
    }
}
