<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\GetProduceService;

class GetManufactureOrder extends Command
{
    protected $produceService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'manufacture-order:get {so_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get manufacture orders from T9 API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(GetProduceService $produceService)
    {
        parent::__construct();
        $this->produceService = $produceService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->produceService->getManufactureOrder($this->argument('so_id'));   
    }
}
