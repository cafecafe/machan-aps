<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Services\MachanService;

class SynchronizePerson extends Command
{
    protected $machanService;

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'person:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronize person data from web service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MachanService $machanService)
    {
        parent::__construct();
        $this->machanService = $machanService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->machanService->syncPerson();
    }
}
