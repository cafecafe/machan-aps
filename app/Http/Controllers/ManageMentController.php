<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ManageMentRepository;
use App\Services\AssignTypeService;

class ManageMentController extends Controller
{
    protected $manageRepo;
    protected $assingService;

    public function __construct(ManageMentRepository $manageRepo, AssignTypeService $assingService)
    {
        $this->manageRepo = $manageRepo;
        $this->assingService = $assingService;
    }

    public function index()
    {
        return view('management.index');
    }

    public function edit($id)
    {
        $data = $this->manageRepo->find($id);

        return view('management.edit', ['data' => $data]);
    }

    public function create()
    {
        return view('management.create');
    }

    public function store()
    {
        $data = $this->manageRepo->create(request()->only('account', 'name', 'role_id'));

        if ($data) {
            return redirect()->route('management.index');
        }
    }

    public function update($id)
    {
        $this->manageRepo->update($id, request(['account', 'name', 'role_id']));

        return redirect()->route('management.index');
    }

    public function destroy($id)
    {
        $this->manageRepo->destroy($id);

        return redirect()->route('management.index');
    }

    public function managementIndex()
    {
        $data = $this->manageRepo->managementIndex(request()->amount);

        return $data;
    }

    public function getRole()
    {
        return $this->assingService->role();
    }

    public function getOrganization()
    {
        return $this->assingService->organization();
    }
}
