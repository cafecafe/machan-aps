<?php

namespace App\Http\Controllers;
use App\Repositories\TechRoutingRepository;
use App\Services\MachanService;

use Illuminate\Http\Request;

class TechRoutingController extends Controller
{
    protected $techRepo;
    protected $machanService;
    public function __construct(TechRoutingRepository $techRepo, MachanService $machanService)
    {
        $this->techRepo = $techRepo;
        $this->machanService = $machanService;
    }

    public function syncTechRouting()
    {
        $status = $this->machanService->syncTechRouting();
        return view('techrouting.index',['status'=>$status]);
    }

    public function techRoutingIndex() //index paginate
    {
        $data =  $this->techRepo->techRoutingIndex(request()->amount);
        return $data;
    }

    public function index()
    {
        return view('techrouting.index',['status'=>true]);;
    }

    public function edit($id)
    {
        $data = $this->techRepo->edit($id);
        return view('techrouting.edit' , ['datas' => $data]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->only([
            'tech_routing_id',
            'tech_routing_name',
            'assign_work',
            'standard_pre_time',
            'standard_tct',
            'change_time',
            'device_multiple',
            'min_unable_order_time',
            'default_resource'
        ]);

        $this->techRepo->update($data, $id);

        return redirect('/tech-routing');
    }
}
