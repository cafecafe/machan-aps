<?php

namespace App\Http\Controllers;

use App\Repositories\RoleSetRepository;

class RoleSetController extends Controller
{

    protected $roleSetRepo;

    public function __construct(RoleSetRepository $roleSetRepo)
    {
        $this->roleSetRepo = $roleSetRepo;
    }

    public function index()
    {
        return view('roleset.index');
    }

    public function create()
    {
        return view('roleset.create');
    }

    public function store()
    {
        $this->roleSetRepo->store(request()->only('org_select', 'routing_select'));
        return redirect('role-set');
    }

    public function edit($id)
    {
        $data = $this->roleSetRepo->find($id);
        return view('roleset.edit', ['data' => $data]);
    }

    public function update($id)
    {
        $this->roleSetRepo->update($id, request()->only('org_select', 'routing_select'));
        return redirect('role-set');
    }

    public function destroy($id)
    {
        $this->roleSetRepo->destroy($id);
        return redirect('role-set');
    }

    public function getRoleSet()
    {
        return $this->roleSetRepo->getRoleSet(request()->amount);
    }
}
