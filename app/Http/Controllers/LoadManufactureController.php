<?php

namespace App\Http\Controllers;

use App\Entities\Manufacture;
use Illuminate\Http\Request;

class LoadManufactureController extends Controller
{
    public function index(){
        return view('searchdataload/search-manufacture-loaded');
    }
    
    public function searchload(){
        $data = request()->all(['so_id','date']);
        $result = Manufacture::when($data['so_id'],function($query,$so){
            $query->where('so_id',$so);
        })
        ->when($data['date'],function($query,$date){
            $query->where('created_at','like','%'.$date.'%');
        })
        ->with('relatedTechRoute')
        ->orderBy('so_id')
        ->orderBy('complete_date', 'asec');
        return response()->json($result->paginate(request()->amount));
        
    }
}
