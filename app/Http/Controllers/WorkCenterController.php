<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\WorkCenterRepository;
use App\Repositories\ResourceSetRepository;
use App\Services\MachanService;

class WorkCenterController extends Controller
{
    protected $workcenterRepo;

    public function __construct(WorkCenterRepository $workcenterRepo, MachanService $machanService,
                                 ResourceSetRepository $resRepo)
    {
       $this->workcenterRepo = $workcenterRepo;
       $this->machanService = $machanService;
       $this->resRepo = $resRepo;
    }

    public function index()
    {
        return view('system/workcenter',['status' => true]);
    }

    public function getCenterData() //if remote api updated, how to update local DB
    {
        $status = $this->machanService->getWorkCenter();

        return view('system.workcenter',['status' => $status]);
    }

    public function getCenter() //get select workcenter_id
    {
        $params = request()->value;
        $data = $this->workcenterRepo->getCenter($params);

        return response()->json($data);
    }

    public function getIndexData()
    {
        $data = $this->workcenterRepo->getIndexData(request()->amount);

        return response()->json($data);
    }

    public function addDefaultResource()
    {
        $data = $this->workcenterRepo->getWorkCenter();
        $this->resRepo->addDefaultResource($data);
        return redirect()->back();
    }
}
