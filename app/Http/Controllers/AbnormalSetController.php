<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SetupExceptionRepository;

class AbnormalSetController extends Controller
{
	protected $workRepo;

	public function __construct(SetupExceptionRepository $workRepo) {
		$this->workRepo = $workRepo;
	}

    public function index()
    {
        return view('system/abnormalset');
    }

    public function controll_page($id = 0 ) {
        $entity = '';
        if ($id == 0 ) {
    	   return view('system/abnormalset_ctrl',['entity' => $entity]);
        } else {
            $entity = $this->workRepo->get($id);
            return view('system/abnormalset_ctrl')
                    ->with(['entity'=> $entity[0],'id'=> $id]) ;
        }
    }

    public function edit($id) {

        $this->workRepo->fix($id, request()->only('Type', 'Exception','EX_Code'));
        return redirect('abnormal-set');
    	// ExeceptionOriginRepository::edit($request);
    }

    public function delete($id) {
        $this->workRepo->delete($id);
      	return redirect()->back();
    }

    public function add() {
    	$this->workRepo->add(request());
    	return redirect('/abnormal-set');
    }

    public function getData(){
    	$result = $this->workRepo->print();
    	return response()->json($result);
    }
}
