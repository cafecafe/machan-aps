<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entities\ParentPart;
use App\Entities\ChildPart;
use App\Entities\SaleOrder;
use App\Entities\Manufacture;

class BomController extends Controller
{

    public function index()
    {
        return view('dataload/search-bom');
    }

    public function resultBom()
    {
        $data = request()->only('condition', 'parent');
        return view('dataload/result-bom', ['condition' => $data['condition']], ['parent' => $data['parent']]);
    }

    public function getBomData()
    {
        $data = request()->only('condition', 'parent');
        if ($data['condition'] == 1) {
            $result = ParentPart::where('material_id', 'like', $data['parent'].'%');
        } elseif ($data['condition'] == 2) {
            $result = SaleOrder::where('so_id', $data['parent'])->with('parentParts');
        } elseif ($data['condition'] == 3) {
            $result = Manufacture::where('mo_id', 'like', $data['parent'].'%');
        } else {
            return redirect()->route('search-bom')->with('message', 'The parameter undefine');
        }
        return response()->json($result->paginate(request()->amount));
    }

    public function listBom($data)
    {
        $result = ParentPart::where('material_id', $data)->first();
        return view('dataload/list-bom', ['result' => $result ]);
    }

    public function getComBom()
    {
        $data = request('data');
        $parent = ParentPart::where('material_id', $data)->first();
        $child = ChildPart::where('top_id', $parent->id)->with('parent')->get();
        return response()->json($child);
    }
}
