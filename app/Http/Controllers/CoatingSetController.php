<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\MachanService;
use App\Repositories\SetupCoatingRepository;
use App\Entities\SetupCoating;
use App\Services\CoatingService;

class CoatingSetController extends Controller
{

	protected $coatRepo ;
	public function __construct(SetupCoatingRepository $coatRepo, MachanService $machanService,CoatingService $coatService) 
    {
		$this->coatRepo = $coatRepo;
        $this->machanService = $machanService;
        $this->coatS = $coatService;
	} 
    public function index () 
    {
    	return view('/system/setup_coating');
    }
    public function store () 
    {
    	$data = request()->only(['cu_pc_material_id', 'cu_p_zone', 'cu_is_cover', 'cu_p_cell_hook_num','cu_rpm','cu_a_hookm_num']);
        $service = $this->coatS;
        
        $data['cu_p_cell_hook_num'] = $this->limit_range($data['cu_p_cell_hook_num'], 100, 1);
        $data['cu_rpm'] = $this->limit_range($data['cu_rpm'], 2000, 100);
        $data['cu_a_hookm_num'] = $this->limit_range($data['cu_a_hookm_num'], 100, 1);

        $data['cu_org_id'] ='1000';
        // $data['cu_ptotalhook'] = $service->getPTotalHook($data['cu_pzone']);
        $data['cu_p_total_len'] = $service->getPTotalLen($data['cu_p_zone']);
        $data['cu_p_def_rate'] = $service->getDefRate($data['cu_rpm']);
        $data['cu_change_distance'] = $service->getChangeDistance($data['cu_p_zone'],$data['cu_is_cover']);
        $data['cu_change_time'] = $service->getChangeTime($data['cu_p_zone'],$data['cu_is_cover'],$data['cu_rpm']);
        $data['cu_ct'] = $service->getCTTime($data['cu_p_cell_hook_num'],$data['cu_a_hookm_num'],$data['cu_rpm']);
        $data['cu_tct'] = $service->getTCT($data['cu_p_zone'],$data['cu_rpm']);
    	$this->coatRepo->new($data);
    	
        return redirect()->back();
    }

    public function update($id) 
    {

    	$this->coatRepo->fix($id,request()
    		->only(['cu_pc_material_id', 'cu_p_zone', 'cu_is_cover', 'cu_p_cell_hook_num','cu_rpm','cu_a_hookm_num']));
    	return redirect()->back();

    }
    public function destroy($id)
    {

    	$this->coatRepo->delete($id);
    	return redirect()->back();
    }

    //用於提供前端資料總輸出
    public function getCoatingData () 
    {
        $data = $this->coatRepo->print(request()->amount);
        return response()->json($data);
    }

    //用於提供前端單筆編輯資料
    public function getCoatingItem () 
    {

        $data = $this->coatRepo->get(request()->id);
        return response()->json($data);
    }

    //用於同步遠端資料
    public function SyncToRemote() 
    {
        $data = $this->machanService->getCoating();
            foreach ($data as $key => $value) {
                $calcul['tct'] = $this->coatS->getTCT($value->CU_PZone,$value->CU_RPM);
                $this->coatRepo->sync($value,$calcul);
            }
        return redirect()->back();
        
    }   
    public function limit_range($v,$max,$min) {
        $v = max($v,$min);
        $v = min($v,$max);
        return $v;
    }
}
