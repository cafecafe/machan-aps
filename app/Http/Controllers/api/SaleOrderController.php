<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Entities\Schedule;

class SaleOrderController extends Controller
{
    public function getSaleOrder()
    {
        $data = request(['online_date', 'sale_order', 'customer']);
        if ($data['online_date'] == null && $data['sale_order'] == null && $data['customer'] == null) {
            return [];
        }
        $result = Schedule::when($data['sale_order'], function ($query, $saleOrder) {
            $query->where('so_id', 'like', '%'.$saleOrder.'%');
        })
        ->when($data['customer'], function ($query, $customer) {
            $query->where('customer', 'like', '%'.$customer.'%');
        })
        ->when($data['online_date'], function ($query, $date) {
            $query->whereDate('date', $date);
        });
        $result->with('saleOrder')
            ->with('parentParts.downstreamChild.parent');
        return response()->json($result->get());
    }
}
