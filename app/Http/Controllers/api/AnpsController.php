<?php

namespace App\Http\Controllers\api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\AnpsRepository;
use App\Services\MachanService;

class AnpsController extends Controller
{
    protected $machanService;
    protected $anpsRepo;

    public function __construct(MachanService $machanService, AnpsRepository $anpsRepo)
    {
        $this->machanService = $machanService;
        $this->anpsRepo = $anpsRepo;
    }

    public function syncANPS()
    {
        $data = $this->machanService->syncANPS();
        if ($data == 0) {
            return response()->json(['status' => '0', 'message' => 'success']);
        } else {
            return response()->json(['status' => '1', 'message' => 'error']);
        }
    }
}
