<?php

namespace App\Http\Controllers;

use App\Http\Requests\SearchSaleOrder;
use App\Services\GetProduceService;
use App\Services\SimulationTransferService;
use App\Repositories\SimulationSchemeRepository;
use App\Repositories\ResourceSetRepository;
use Illuminate\Support\Facades\Auth;

class SimulationSchemeController extends Controller
{
    protected $produceService;
    protected $simulationRepo;

    public function __construct(GetProduceService $produceService, SimulationSchemeRepository $simulationRepo,
                                ResourceSetRepository $Res,
                                 SimulationTransferService $simuService)
    {
        $this->produceService = $produceService;
        $this->simuService = $simuService;
        $this->simulationRepo = $simulationRepo;
        $this->Res = $Res;
        $this->status = ['已生效', '已模擬' , '已確認', '已排單'];
    }

    public function sourceOrder()
    {
        return view('simulation.source-order');
    }

    public function sourceOrderResult()
    {
                
        return view('simulation.source-order-result');
    }

    public function getSourceOrderData(SearchSaleOrder $request)
    {
        $data = request([
            'org_id', 'container_date_start',
            'container_date_end', 'bill_date_start',
            'bill_date_end', 'so_id', 'customer_name',
            'amount', 'page','status','added'
        ]);
        // dd($data);
        if($data['added'] == 0) {
            // dd(1);
            $this->simulationRepo->NormalizeOrder();
        }
        $billStratDate = date('Ymd', strtotime($data['bill_date_start']));
        $billEndDate = date('Ymd', strtotime($data['bill_date_end']));
        $this->produceService->getSourceOrderData(
            $data['org_id'],
            $billStratDate,
            $billEndDate,
            $data['container_date_start'],
            $data['container_date_end'],
            $data['so_id'],
            $data['customer_name']
        );
        return redirect()->route('source-order-result', [
            'org_id' => $data['org_id'],
            'bill_date_start' => $billStratDate,
            'bill_date_end' => $billEndDate,
            'container_date_start' => $data['container_date_start'],
            'container_date_end' => $data['container_date_end'],
            'so_id' => $data['so_id'],
            'customer_name' => $data['customer_name'],
            'status' => $data['status']
        ]);
    }
    public function getManufactureOrderData() //get manufacture
    {
        $datas = request()->only(['so_id','scheme_case']);
        $this->simulationRepo->NormalizeOrder($datas['so_id']);
        $datas['user'] = Auth::user()->account;
        $this->produceService->getManufactureOrderData($datas['so_id']);

        return view('simulation.manufacture-order', $datas);
    }

    public function manufactureOrderResult()
    {
        return $this->simulationRepo->manufactureOrderResult(request()->amount);
    }

    public function getLoadSourceOrder() //add simulation SO page
    {
        $data = request()->all(
            'bill_date_start',
            'bill_date_end',
            'org_id',
            'container_date_start',
            'container_date_end',
            'customer_name',
            'so_id',
            'status'
        );
        return response()
            ->json($this->simulationRepo->loadedSourceOrder($data)
            ->paginate(request()->amount));
    }
    public function generateSimulation()
    {   
        // dd(request()->all());
        $datas = request()->only('datas','user','scheme_case','assign_work');
        $scheme_id = $this->simuService->generateSimulation($datas);
        if($scheme_id === 'full')    
            return view('error')->with('message','模擬排程編碼用盡 !');
        else return view('simulation.generate-scheme-result')
                    ->with(['scheme_id' => $scheme_id]);
    }
    public function getGenerateSimulation()
    {
        return $this->simulationRepo->getGenerateSimulation(request()->only(['amount', 'scheme_id']));
    }
    public function searchScheme()
    {
        return view('simulation.search-scheme');
    }

    public function getSearchScheme()
    {
        $params = request()->only('scheme_start','scheme_end','scheme_status','user','scheme_case');
        $result = $this->simulationRepo->getSearchScheme($params);
        $status = $this->status[$params['scheme_status']];
        return view('simulation/search-scheme-form')->with(['datas' => json_decode($result),'status'=>$status]);
    }

    public function searchSchemeResult($scheme_id)
    {
        return view('simulation.search-scheme-result')->with(['scheme_id' => $scheme_id]);
    }

    public function getSearchSchemeResult()
    {
        # code...
    }
    public function showLoadForm()  
    {   
        //取得製令資訊
        $datas = request()->datas;

        //若沒有勾選製令，跳轉至錯誤頁面
        if(!$datas) 
            return view('error',['message' => "沒有資料分析，請勾選" ]);

        //訂單群組陣列
        $so_datas =[];

        //統計結果陣列
        $form = [];

        //排程時間選項
        $aps_opt = "scheme_recommend_lastest_";

        //機台陣列
        $machine = [];

        //取得勾選製令中統計時間選項最晚之製令日期
        $latest_date = substr(collect($datas)->max($aps_opt."end"),0,10);

         //取得勾選製令中統計時間選項最早之製令日期
        $earliest_date = substr(collect($datas)->min($aps_opt."start"),0,10);

        //取得上述兩者之日期差距
        $days = (strtotime($latest_date) - strtotime($earliest_date))/(3600*24);

        //建立訂單群組陣列，第一維是訂單，第二維是所屬該訂單之製令
        foreach($datas as $key => $value) {
            $so_datas[$value['so_id']][] = $value;
        }

        //建立統計結果陣列，在有限範圍內賦予陣列元素預設值為零
        for ($i=0; $i <= $days;$i ++) { 
            for ($j=0; $j < 8; $j++) { 
                if(!isset($form[$i][$j])) $form[$i][$j] =0;
            }
        }

        //aps代碼對應之陣列索引值
        $aps = [
            "50" => 1,
            "40" => 2,
            "30" => 3,
            "20" => 4,
            "13" => 5,
            "12" => 6,
            "11" => 7
        ];

        //工作日一天之開始時間，未來根據統計當日之工作日調整此參數
        $day_end = strtotime("17:20") %(3600*24);

        //工作日一天之結束時間，未來根據統計當日之工作日調整此參數，
        $day_start =strtotime("8:00") %(3600*24) ;

        //一日工作時間，未來請用上述兩者相減算出，單位為秒，以下代表8小時
        $work_time = 480*60;

        // collect($so_datas)->sortBy($aps_opt."end");

        //取得最晚製令完工日期
        $end_overll_date = strtotime(
            date("Y-m-d",strtotime(collect($datas)->max($aps_opt."end"))));

        //開始統計，一次統計一筆訂單的所有製令，下筆訂單統計之前，前一筆訂單的製令已統計完成
        foreach ($so_datas as $k => $mo) {
            $temp['aps_id'] ='';
            for ($i=0; $i < count($mo) ; $i++) { 
             
                //排程時間選項定義之該製令結束執行日
                $end_date = strtotime(date("Y-m-d",strtotime($mo[$i][$aps_opt."end"])));
             
                //排程時間選項定義之該製令開始執行日
                $start_date = strtotime(date("Y-m-d",strtotime($mo[$i][$aps_opt."start"])));
             
                //如果統計製令的aps製程碼不等於上一個統計製令的aps製程碼
                if($mo[$i]['aps_id'] != $temp['aps_id']){

                    //根據製程碼取得機台陣列中的對應索引值
                    $m = $aps[substr($mo[$i]['aps_id'],0,2)];
                    
                    //查詢該製令的資源資訊，取得該資源的相關欄位
                    $res = $this->Res->getResourceInfo($mo[$i]['resource_id']);

                    //將資源欄位資訊存於機台陣列
                    $machine[$m]['resource_id'] = $res->resource_id;
                    $machine[$m]['work_name'] = $res->workcenter->workcenter_name;
                    $machine[$m]['man_power'] = $res->device_multiple;
                    $machine[$m]['aps_id'] = $res->workcenter->routing_level;
                }
                //取得製令開始日和製令結束日之時間差
                $i_day = ($end_date - $start_date) / (3600*24) ;

                //統計時間開始點修正，統計時間開始點為與最晚製令完工日的日差距
                $d_start = ($end_overll_date -  $end_date)/ (3600*24);

                //開始統計，以某筆訂單中製令為最小單位，統計該製令所占時日
                //form陣列第一維為以日為單位的時間點，第二維第一元素為該日期字串，後面元素為不同機台當日的統計
                //form第一維索引值大小與時間早晚相關，索引值越小時間越晚
                for ($d= $d_start; $d <= $i_day+$d_start; $d++) {

                    //將日期字串存入時間點的第一元素
                    $form[$d][0] = date("m/d",$end_overll_date - 3600*24*($d));

                    //當時間點為最晚
                    if($d==0){

                        //當製令開始與結束不同天
                        if($start_date != $end_date)
                            //取得排程時間選項時戳計算最晚當天所佔時間
                            $h_time = strtotime($mo[$i][$aps_opt."end"])%(3600*24) - $day_start;

                        //當製令開始與結束同天
                        else{  
                            //只需統計製令當日完工時間減去製令當日開始時間
                            $h_time = strtotime($mo[$i][$aps_opt."end"]) -strtotime($mo[$i][$aps_opt."start"]);
                        }
                        //將此日統計在統計結果陣列，結果為當日時間量除以工作時間之百分比
                        $form[$d][$m] += intval(($h_time/$work_time)*100) ;
                    }
                    //當統計時間點到達最早
                    else if($d == $i_day+$d_start){
                        //取得排程時間選項時戳計算最早當天所佔時間
                        $f_time = $day_end - strtotime($mo[$i][$aps_opt."start"])%(3600*24) ;

                        //將此日統計在統計結果陣列，結果為當日時間量除以工作時間之百分比
                        $form[$d][$m] += intval(($f_time/$work_time)*100) ;
                    }
                    else {
                        //在製令開始日與製令結束日之間的時間點一律為100%負荷
                        $form[$d][$m] += 100;
                    }

                }

                //紀錄本次的製令，供下次製令比對資訊
                $temp = $mo[$i];
            }
        }
        //將沒用到機台陣列索引設為零
        for ($i=0; $i < 8 ; $i++) { 
            if(!isset($machine[$i]))  $machine[$i] =0;
        }
        //collect($machine)->sortKeys();
        return view('simulation.scheme-load-form',[ 'form' =>$form ,'machine'=>$machine]);
    }

    public function confirmMoStatus()
    {
        $data = request()->only(["confirmmo"]) ;
        $mo = explode(",",$data["confirmmo"]);
        $this->simulationRepo->confirmMoStatus($mo);

    }

    public function issueMoStatus()
    {
        $data = request()->only(["issuemo"]) ;
        $mo = explode(",",$data["issuemo"]);
        $this->simulationRepo->issueMoStatus($mo);

    }
    public function destroySale()
    {
        $data = request()->only(['id','search']);
        $rm_id = array_unique($data['id']);
        if(!$rm_id) return redirect()->back();
        $this->simulationRepo->destroySale($rm_id);  
        return redirect("simulation/source-order-result/".$data['search']);
    }
}
