<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ResourceSetRepository;
class ResourceSetController extends Controller
{
    protected $resRepo;

    public function __construct(ResourceSetRepository $resRepo)
    {
        $this->resRepo = $resRepo;
    }

    public function index()
    {
        return view('system/resourceset');
    }

    public function create()
    {
        return view('system/createsourceset');
    }

    public function store(Request $request)
    {
        $data = request()->only([
            'resource_id',
            'resource_name',
            'resource_type',
            'device_id',
            'device_name',
            'device_qty',
            'workcenter_id',
            'assign_work',
            'standard_pre_time',
            'standard_time',
            'standard_tct',
            'change_time',
            'device_multiple',
            'paint_length',
            'hook_qty',
            'change_color_time',
            'standard_change_time',
            'standard_rate',
            'empty_hook_distance',
            'is_default'
        ]);
        $result = $this->resRepo->create($data);
        if (!$result) {
            return back()->with('message', '已有預設的資源中心');
        }
        return redirect('resource');
    }

    public function destroy($id)
    {
        $this->resRepo->destroy($id);

        return redirect()->route('resource.index');
    }

    public function edit($id)
    {
        $data = $this->resRepo->find($id);
        $shift =$data->shift->name;
        if (!$data) {
            return back();
        }
        return view('system/editresourceset', ['data' => $data ,'shift' => $shift ]);
    }

    public function update(Request $request, $id)
    {
        $data = $request->only([
            'resource_id', 'resource_name', 'resource_type', 'device_id', 'device_name',
            'device_qty', 'workcenter_id', 'assign_work', 'standard_pre_time',
            'standard_time', 'standard_tct', 'change_time', 'device_multiple',
            'paint_length', 'hook_qty', 'change_color_time', 'standard_change_time',
            'standard_rate', 'empty_hook_distance', 'is_default'
        ]);
        $this->resRepo->update($id, $data);

        return redirect()->route('resource.index');
    }

    public function getResData() //get relationship paginate
    {
        $data = $this->resRepo->getResData(request()->amount);

        return response()->json($data);
    }

    public function getResource()
    {
        $data = $this->resRepo->getResource();

        return response()->json($data);
    }
}
