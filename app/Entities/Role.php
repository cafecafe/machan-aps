<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = [
        'name', 'factory_id', 'routing_level'
    ];
	protected $attributes = [
		'factory_id' => 1,
		'routing_level' => 1
	];
}
