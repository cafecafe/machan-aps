<?php

namespace App\Entities;

use Illuminate\database\Eloquent\model;

class SetupCoating extends model
{
   	protected $fillable = [
		'cu_p_id',
		'cu_p_material_id',
		'cu_pc_material_id',
		'cu_p_zone',
		'cu_p_total_len',
		'cu_p_total_hook',
		'cu_is_cover',
		'cu_a_hookm_num',
		'cu_a_material_h_num',
		'cu_rpm',
		'cu_p_cell_hook_num',
		'cu_hook_no',
		'cu_cuft',
		'cu_p_hook_num',
		'cu_p_def_rate',
		'cu_ct',
		'cu_change_distance',
		'cu_change_time',
		'cu_org_id',
		'cu_tct',
   	];
}
