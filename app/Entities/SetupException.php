<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class SetupException extends Model
{
    protected $fillable = [
    	'Type',
    	'EX_Code',
    	'Exception',
    ];

}
