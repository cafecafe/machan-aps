<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class WorkCenter extends Model
{
    protected $fillable = [
        'factory_id',
        'workcenter_id',
        'workcenter_name',
        'org_id',
        'transfer_factory',
        'factory_type',
        'work_level',
        'routing_level',
        'aps_id',
        'is_subcontract_factory',
        'subcontract_partner',
        'person_id',
        'off_set_ware_houseid',
    ];

    public function resource()
    {
        return $this->hasMany('App\Entities\Resource', 'workcenter_id', 'workcenter_id');
    }

    public function routing_level()
    {
        return $this->hasOne('App\Entities\Resource', 'routing_level', 'routing_level');
    }

}
