<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    protected $fillable = [
        'date', 'type', 'so_id', 'complete_date', 'item_id', 'item_name', 'qty', 'batch', 'org_id', 'customer', 'sync_check'
    ];

    public function parentParts()
    {
        return $this->hasMany('App\Entities\ParentPart', 'material_id', 'item_id');
    }

    public function saleOrder()
    {
        return $this->hasMany('App\Entities\SaleOrder', 'so_id', 'so_id');
    }
}
