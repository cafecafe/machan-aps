<?php

namespace App\Repositories;

use App\Entities\WorkCenter;

class WorkCenterRepository
{
    public function getData(array $data)
    {
        $org_id = [
            "1" => '10',
            "2" => '20',
            "3" => '30',
            "4" => '40',
            "5" => '60',
            "6" => '50',
        ];
        $transF = [
            "0" => '0',
            "1" => '1',
            "2" => '2',
            "3" => '3',
            "4" => '4',
            "5" => '5',
            "6" => '6',
            "7" => '',
            "8" => '5',
        ];
        $work_level = [
            "0" => "10",
            "1" => "20",
            "2" => "30",
            "3" => "40",
            "4" => "50",
            "5" => '50'
        ];
        $routing_level = [
            "0" => "1",
            "1" => "3",
            "2" => "2",
            "3" => "4"
        ];
        foreach ($data as $key => $workCenter) {
            if (!isset (
                $workCenter->FactoryId,
                $workCenter->WorkCenterId,
                $workCenter->WorkCenterName,
                $workCenter->IsSubcontractFactory,
                $workCenter->SubcontractPartner,
                $workCenter->PersonId,
                $workCenter->OffsetWarehouseId,
                $workCenter->CU_APSState
            )) continue;

            if(!isset($workCenter->IsSubcontractFactory, $workCenter->CU_APSState, $workCenter->WorkCenterId)) {
                continue;
            }
            if (!$workCenter->IsSubcontractFactory && $workCenter->CU_APSState == 0 && $workCenter->WorkCenterId[1] != 8) { //判斷是否委外
                if(!isset($org_id[$workCenter->WorkCenterId[0]])){
                    continue;
                }else{
                    $workCenter->org_id = $org_id[$workCenter->WorkCenterId[0]];
                }

                if (!isset($transF[$workCenter->WorkCenterId[1]])) {
                    continue;
                } else {
                    $workCenter->transfer_factory = $transF[$workCenter->WorkCenterId[1]];
                }

                $workCenter->factory_type = (int)$workCenter->org_id+(int)$workCenter->transfer_factory;
                if (! isset($work_level[$workCenter->WorkCenterId[2]])) {
                    continue;
                } else {
                    $workCenter->work_level = $work_level[$workCenter->WorkCenterId[2]];
                }
                if (isset($workCenter->WorkCenterId[4])) {
                    $workCenter->routing_level = (int)$routing_level[$workCenter->WorkCenterId[4]] + (int)$workCenter->work_level;
                }
                else if ($workCenter->WorkCenterId[2] == '0') {
                    $workCenter->routing_level = (int)$workCenter->work_level + 1;
                }
                else {
                    $workCenter->routing_level = $workCenter->work_level;
                }
                if ($workCenter->work_level == '') {
                    $workCenter->aps_id = '';
                }
                else {
                    $workCenter->aps_id = $workCenter->routing_level.$workCenter->factory_type;
                }

                WorkCenter::updateOrCreate (
                    [
                        'workcenter_id' => $workCenter->WorkCenterId,
                    ],
                    [
                        'factory_id' => $workCenter->FactoryId,
                        'workcenter_name' => $workCenter->WorkCenterName,
                        'is_subcontract_factory' => $workCenter->IsSubcontractFactory,
                        'person_id' => $workCenter->PersonId,
                        'off_set_ware_houseid' => $workCenter->OffsetWarehouseId,
                        'org_id' => $workCenter->org_id,
                        'transfer_factory' => $workCenter->transfer_factory,
                        'factory_type' => $workCenter->factory_type,
                        'work_level' => $workCenter->work_level,
                        'routing_level' => $workCenter->routing_level,
                        'aps_id' => $workCenter->aps_id,
                    ]
                );
            }
        }
    }

    public function getCenter($params)
    {
        if (!$params) {
            $params = '';
        }
        $data = WorkCenter::where('routing_level', $params)->select('id', 'workcenter_id', 'routing_level')->get()->all();
        return $data;
    }

    public function getIndexData($amount)
    {
        return WorkCenter::paginate($amount);
    }

    public function getWorkCenter() //get ResourceSet need all WorkCenter
    {
        return WorkCenter::select('workcenter_id','org_id', 'routing_level')->get();
    }
}

