<?php

namespace App\Repositories;

use App\Entities\User;

class UserRepository
{
    public function create(array $personInfo)
    {
        collect($personInfo)->each(function ($data, $index) {
            User::firstOrCreate(
                [
                    'account' => $data->PersonId
                ],
                [
                    'name' => $data->PersonName,
                    'password' => bcrypt($data->PersonId),
                    'role_id' => 1,
                ]
            );
        });
    }
}
