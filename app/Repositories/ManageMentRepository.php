<?php
namespace App\Repositories;

use App\Entities\User;
use App\Entities\Organization;

class ManageMentRepository
{
    public function managementIndex($page)
    {
        return User::with('role')->select('id', 'account', 'name', 'role_id')->paginate($page);
    }

    public function find($id)
    {
        $data = User::with('role')->where('id', $id)->first(['id', 'account', 'name', 'role_id']);

        if (!$data) {
            return false;
        } else {
            return $data;
        }
    }

    public function create(array $data)
    {
        $data['password'] = bcrypt($data['account']);
        return User::create($data);
    }

    public function update($id, array $request)
    {
        $data = User::find($id);

        if (!$data) {
            return false;
        } else {
            return $data->update($request);
        }
    }

    public function destroy($id)
    {
        $data = User::find($id);

        if (!$data) {
            return false;
        } else {
            return User::destroy($id);
        }
    }
}
