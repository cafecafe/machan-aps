<?php

namespace App\Repositories;

use App\Entities\SetupCoating;

class SetupCoatingRepository
{
    
    public function new($info) 
    {
                // dd($info);
        $info['cu_pid'] = "0";
        $info['cu_p_materialid'] = "0";
        SetupCoating::updateOrCreate(
        [            
            'cu_pc_material_id' => $info['cu_pc_material_id']
        ],$info);
        // if($boolean) {
        //     SetupCoating::create($info);
        // }
        // else {
        //     //update data
        // }

    }

    public function sync($info,$calcul="") 
    {

        SetupCoating::updateOrCreate(
        [
            'cu_p_id' => $info->CU_PId
        ],
        [
            'cu_p_material_id' => $info->CU_PMaterialId,
            'cu_pc_material_id' => $info->CU_PCMaterialId,
            'cu_p_zone' => $info->CU_PZone,
            'cu_p_total_len' => $info->CU_PTotalLen,
            'cu_p_total_hook' => $info->CU_PTotalHook,
            'cu_is_cover' => $info->CU_IsCover,
            'cu_a_hookm_num' => $info->CU_AHookMNum,
            'cu_a_material_h_num' => $info->CU_AMaterialHNum,
            'cu_rpm' => $info->CU_RPM,
            'cu_p_cell_hook_num' => $info->CU_PCellHookNum,
            'cu_hook_no' => $info->CU_HookNo,
            'cu_cuft' => $info->CU_CUFT,
            'cu_p_hook_num' => $info->CU_PHookNum,
            'cu_p_def_rate' => $info->CU_PDefRate,
            'cu_ct' => $info->CU_CT,
            'cu_change_distance' => $info->CU_ChangeDistance,
            'cu_change_time' => $info->CU_ChangeTime,
            'cu_org_id' => $info->CU_OrgId,
            'cu_tct' => $calcul['tct']

         ]
        );

    }

    public function fix($id,$data) 
    {
        SetupCoating::where('id', $id)->update($data);
    }

    public function get($id) 
    {
        return SetupCoating::where('id', $id)->get();
    }

    public function delete($id) 
    {
        SetupCoating::where('id', $id)->delete();
    }

    public function print($amount) 
    {
    	return  SetupCoating::orderBy('created_at','desc')->paginate($amount);
    }
}
