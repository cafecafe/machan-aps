<?php

namespace App\Repositories;

use App\Entities\ParentPart;
use App\Entities\ChildPart;

class BomRepository
{
    public function __construct()
    {
        $this->t = [];
    }
    public function synchronize(array $data, $itemId, $getBom)
    {
        is_null($getBom) ?: $this->t = $getBom;
        $firstData = $data[0];
        $parentCom = ParentPart::updateOrCreate(
            ['material_id' => $firstData->MaterialId],
            [
                'bomkey_id' => $firstData->BOMKeyId,
                'bomkey_name' => $firstData->BOMKeyName,
                'unit_id' => $firstData->UnitId,
                'techroutekey_id' => $firstData->TechRouteKeyId,

            ]
        );

        $level = ChildPart::where('down_id', $parentCom->id)
            ->where('item_id', $itemId)
            ->first()->level ?? 0;

        collect($data)->each(function ($bom) use ($parentCom, $itemId, $level) {
            $childCom = ParentPart::firstOrCreate(
                ['material_id' => $bom->SubMaterialId],
                [
                    'fetch_type' => $bom->FetchType,
                ]
            );
            ChildPart::firstOrCreate(
                ['top_id' => $parentCom->id, 'down_id' => $childCom->id, 'item_id' => $itemId,],
                [
                    'row_no' => $bom->RowNo,
                    'row_id' => $bom->RowId,
                    'unit_id' => $bom->UnitId1,
                    'unit_qty' => $bom->UnitQty,
                    'nuse_qty' => $bom->NUseQty,
                    'base_qty' => $bom->BaseQty,
                    'remark' => $bom->Remark,
                    'level' => $level + 1,
                ]
            );
            $t = ParentPart::where('fetch_type', '<>', 0)
                    ->where('material_id', $bom->SubMaterialId)
                    ->first();
            is_null($t) ?: array_push($this->t, $t);
        });
        return $this->t;
        // return $this->checkBomLevel($data, $itemId);
    }

    public function checkBomLevel($data, $itemId)
    {
        return ParentPart::where('fetch_type', '<>', 0)
            ->whereNotExists(function ($query) {
                $query->from('child_parts')
                      ->whereRaw('child_parts.top_id = parent_parts.id');
            })->first();

    }
}

