<?php

namespace App\Repositories;

use App\Entities\Resource;
use App\Entities\WorkCenter;

class ResourceSetRepository
{
    public function find($id)
    {
        return Resource::find($id);
    }
    public function getWorkCenter($resource_id) {
        return Resource::where("resource_id", $resource_id)->workcenter() ;
    }
    public function getResourceInfo($resource_id){
        return Resource::where("resource_id", $resource_id)->first() ;
    }
    public function destroy($id)
    {
        return Resource::destroy($id);
    }

    public function create($parmas)
    {
        $org_id = WorkCenter::where('workcenter_id', $parmas['workcenter_id'])
            ->first()->org_id;
        $parmas['org_id'] = $org_id;
        $status = Resource::where('workcenter_id', $parmas['workcenter_id'])
            ->where('is_default', 1)
            ->exists();
        if (!$status || $parmas['is_default'] === '0') {
            return Resource::create($parmas);
        } else {
            return false;
        }
    }

    public function update($id, $resInfo)
    {
        $paint = !isset($resInfo['paint_length']); //check paint routing
        $res = Resource::find($id);
        if ($res) {
            if ($paint) {
                $resInfo['paint_length'] = 0;
                $resInfo['hook_qty'] = 0;
                $resInfo['change_color_time'] = 0;
                $resInfo['standard_change_time'] = 0;
                $resInfo['standard_rate'] = 0;
                $resInfo['empty_hook_distance'] = 0;
            }
            return $res->update($resInfo);
        }

        return false;
    }

    public function getResData($amount)
    {
        return Resource::with('workcenter')->paginate($amount);
    }

    public function getResource()
    {
        return Resource::get();
    }

    public function addDefaultResource($datas) //get workcenter to add default resource
    {
        $datas->each(function($data) {
            Resource::updateOrCreate(
                [
                    'workcenter_id' => $data->workcenter_id,
                ],
                [
                    'resource_id' => $data->org_id.$data->routing_level.'01',
                    'resource_type' => '設備', //?
                    'resource_name' => $data->org_id,
                    'device_qty' => 1, //?
                    'device_multiple' => 1, //?
                    'assign_work' => 1, //?
                    'standard_time' => 1, //?
                    'standard_pre_time' => 8, //?
                    'change_time' => 10, //?
                    'org_id' => $data->org_id
                ]
            );
        });
    }
}
