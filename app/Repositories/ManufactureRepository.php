<?php

namespace App\Repositories;

use App\Entities\Manufacture;

class ManufactureRepository
{
    public function synchronize(array $data, $batch)
    {
        collect($data)->each(function ($order) use ($batch) {
            if ($order->CurrentState == 4 && $order->ProduceState == 1) {
                // This mo is not working
            } else {
                Manufacture::updateOrCreate(
                    ['mo_id' => $order->BillNo, 'techroutekey_id' => $order->FromTechRouteKeyId],
                    [
                        'item_id' => $order->MaterialId,
                        'item_name' => $order->MaterialName,
                        'customer_name' => $order->BizPartnerName,
                        'qty' => $order->ProduceQty,
                        'online_date' => $order->DemandBeginDate,
                        'so_id' => $order->FromBillNo,
                        'customer' => $order->BizPartnerName,
                        'batch' => $batch,
                        'demand_complete_date' => $order->DemandCompleteDate,
                        'bill_date' => $order->BillDate,
                        'current_state' => $order->CurrentState,
                    ]
                );
            }
        });
    }

    public function appSearchMo($params)
    {
        return Manufacture::select('mo_id')
                ->where('mo_id', 'like', '%'.$params.'%')->distinct()->get();
    }
}
