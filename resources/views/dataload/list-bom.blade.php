@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>BOM 查詢</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">資料載入</span>
            <span class="space-item">></span>
            <span class="space-item">BOM查詢<span>
            <span class="space-item">></span>
            <span class="space-item">資料列表<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="bom-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">產品編號</th>
                        <th scope="col">品名規格</th>
                        <th scope="col">母件基數</th>
                        <th scope="col">子件用量</th>
                        <th scope="col">單位</th>
                        <th scope="col">圖號</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td><a href="#"><span id="ch" class="fa fa-angle-right"></span></a> {{ $result->material_id }}</td>
                        <td>{{ $result->bomkey_name }}</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<script>

    const iconStatus = () => {
        event.preventDefault();
        const target = $(event.target);
        const targetClass = target.attr('class');
        showData(target);
        $(target).attr('class', `fa fa-angle-${targetClass.includes('right') ? 'down'  : 'right'}`);
    }

    $('[id="ch"]').click(iconStatus);

    const showData = (target) => {
        if (target.attr('class').includes('right')) {
            let data = target.parent().parent().text();
            const str = [...Array((data.indexOf(' ') || 1) + 3)].map(() => '&nbsp;').join('');
            data = data.slice(data.indexOf(' ') + 1);
            axios.get('{{ route('get-com-bom') }}', {
                params: { data }
            }).then(({ data }) => {
                data.forEach((comBom, key) => {
                    target.parent().parent().parent().after(`
                        <tr>
                            <td>${str}${key ? '├' : '└'} ${comBom.parent.fetch_type === 0 ? '' : '<a href="#"><span id="ch" class="fa fa-angle-right" onclick="iconStatus()"></span></a> '}${comBom.parent.material_id}</td>
                            <td>${comBom.parent.bomkey_name}</td>
                            <td>${comBom.base_qty}</td>
                            <td>${comBom.unit_qty}</td>
                            <td>${comBom.unit_id}</td>
                            <td>${comBom.remark}</td>
                        </tr>
                    `)
                });
            });
        } else {
            const elements = [];
            let element = target.parent().parent().parent();
            const spaceLength = element.text().includes('├') ? element[0].innerText.indexOf('├') : element[0].innerText.indexOf('└');
            let stauts = spaceLength + 1;
            element = element.next();
            while (stauts > spaceLength) {
                elements.push(element);
                element = element.next();
                if(element.text().includes('├')){
                    stauts = element[0].innerText.indexOf('├');
                }else if(element.text().includes('└')){
                    stauts = element[0].innerText.indexOf('└');
                }else{
                    stauts = -1;
                }
            }
            elements.forEach((element) => {
                element.remove();
            });
        }
    }
</script>
@endsection
