@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>匯入生產進度表</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">資料載入</span>
            <span class="space-item">></span>
            <span class="space-item">匯入生產進度表<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>

        </div>
        <div class="total-data">
            載入筆數 |
            <span id="data-num"></span>
            <div style="float:right; margin-top:-7px">
                <a class="btn btn-success" href="{{ route('load-manufacture-result') }}">產生進度表排程</a>
            </div>
        </div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="saleorder-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">交貨日期</th>
                        <th scope="col">訂單單號</th>
                        <th scope="col">物料代碼</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="amount" onchange="getScheduleData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="10" selected>10</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
    </div>
</div>
<script>
    let lastPage;
    const getScheduleData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('schedule-data') }}', {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            lastPage = data.last_page;
            const orders = data.data;
            $('#data-num').text(`共 ${data.total} 筆`);
            $('#saleorder-data tbody').empty();
            orders.forEach((order, key) => {
                $('#saleorder-data tbody').append(`
                    <tr>
                        <th scope="row">${key + 1 + (page - 1) * amount}</th>
                        <td>${order.date}</td>
                        <td>${order.so_id}</td>
                        <td>${order.item_id}</td>
                    </tr>
                `)
            })
            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getScheduleData(page)
                }
            });
        });
    }
    getScheduleData();
</script>
@endsection
