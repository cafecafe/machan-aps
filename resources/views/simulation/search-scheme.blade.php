@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
    .inpsize {
        zoom:1.5;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>模擬方案查詢</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">模擬方案查詢<span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">載入條件</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="get-search-scheme" method="POST">
                            @csrf
{{--                             <div class="form-group">
                                <label class="col-md-2 control-label">排程方案編號</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="scheme_id" name="scheme_id" required>
                                </div>
                            </div> --}}
                            <div class="form-group">
                                <label class="col-md-2 control-label">模擬日期</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="scheme_start" name="scheme_start"  type="date" >~
                                    <input class="form-control" id="scheme_end" name="scheme_end"  type="date" >
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">排單模擬計畫</label>
                                <div class="col-md-10" id="">
                                    <select class="form-control" id="scheme_case" name="scheme_case" >
                                        <option selected value="0">標準初始模擬</option>
                                        <option value="1">標準模擬</option>
                                        <option value="0">最佳化模擬</option>
                                        <option value="0">標準最佳化模擬</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">排程狀態</label>
                                <div class="col-md-10" id="">
                                    <select class="form-control" id="scheme_status" name="scheme_status" >
                                        <option selected value="1">已模擬</option>
                                        <option value="2">已確認</option>
                                        <option value="3">已排單</option>
                                        <option value="">已回收未排單</option>
                                        <option value="">生產中</option>
                                        <option value="">已出貨</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">排程人(帳號)</label>
                                <div class="col-md-10">
                                    <input class="form-control" id="user" name="user">
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%">載入資料</button>
                                <button type="reset" onclick="resetOption()" class="btn btn-secondary btn-lg" style="width:45%">清除資料</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
