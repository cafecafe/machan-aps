@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
        margin-left:-10px;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
	<div id="page-wrapper">
        <div style="margin-top:15px;">
            {{-- <form action="{{route('confirm-scheme-result')}}" method="GET"> --}}
            <table class="table table-striped table-pos" id="generate-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col" colspan="8" style="text-align: center;">機台</th>
                    </tr>

                </thead>
                <tbody id="form">	
                    <tr>
                    	<th>製程代碼</th>

                    	@for ($i = 1; $i < count($machine) ; $i++)
	                    	@if ($machine[$i] != 0)
	                    		<th>{{$machine[$i]['aps_id']}}</th>
	                    	@else
	                    		<th></th>
	                    	@endif

                    	@endfor
                    </tr>
                    <tr>
                    	<th>工藝路線名稱</th>

                    	@for ($i = 1; $i < count($machine) ; $i++)
	                    	@if ($machine[$i] != 0)
	                    		<th>{{$machine[$i]['work_name']}}</th>
	                    	@else
	                    		<th></th>
	                    	@endif

                    	@endfor
                    </tr>
                    <tr>
                    	<th>資源中心</th>

                    	@for ($i = 1; $i < count($machine) ; $i++)
	                    	@if ($machine[$i] != 0)
	                    		<th>{{$machine[$i]['resource_id']}}</th>
	                    	@else
	                    		<th></th>
	                    	@endif

                    	@endfor
                    </tr>
                    <tr>
                    	<th>人數</th>

                    	@for ($i = 1; $i < count($machine) ; $i++)
	                    	@if ($machine[$i] != 0)
	                    		<th>{{$machine[$i]['man_power']}}</th>
	                    	@else
	                    		<th></th>
	                    	@endif

                    	@endfor
                    </tr>
                	@for ($i = 0; $i < count($form) ; $i++)
                		@if ($form[$i][0]!=0)
	                		<tr>
	                			<td>{{$form[$i][0]}}</td>
	                			<td>{{$form[$i][1]}}%</td>
	                			<td>{{$form[$i][2]}}%</td>
	                			<td>{{$form[$i][3]}}%</td>
	                			<td>{{$form[$i][4]}}%</td>
	                			<td>{{$form[$i][5]}}%</td>
	                			<td>{{$form[$i][6]}}%</td>
	                			<td>{{$form[$i][7]}}%</td>
	                		</tr>
                		@endif
                	@endfor
                </tbody>
            </table>
            <a class="btn btn-secondary btn-lg" href="javascript:history.back()" style="width:45%">返回</a>
        </div>
    </div>
 <script type="text/javascript">

 </script>
@endsection