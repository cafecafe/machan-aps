@extends('layouts.myapp')

@section('css')

<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    hr {
        border-top: 1px solid #ccc;
    }

    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>初始訂單明細</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">排程模擬</span>
            <span class="space-item">></span>
            <span class="space-item">排程來源載入<span>
            <span class="space-item">></span>
            <span class="space-item">初始訂單明細<span>
            <span class="space-item">></span>
            <span class="space-item">初始模擬製令明細<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
        </div>
        <div class="total-data">
            <label>班別</label>
            <select name="assign_work" id="assign_work" form="form">
            </select>
            <label>預設資源中心</label>
            <select>
                <option value="">-------------</option>
            </select>
            <label>指定資源中心</label>
            <select>
                <option value="">-------------</option>
            </select>
        </div>
        <hr>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos" id="saleorder-data">
                <thead class="thead-color">
                    <tr>
                        <th scope="col"><input type="checkbox" name="check_all" id="check_all" onclick="checkAll(this)"></th>
                        <th scope="col">製令單號</th>
                        <th scope="col">母件</th>
                        <th scope="col">來源訂單號</th>
                        <th scope="col">數量</th>
                        <th scope="col">客戶名稱</th>
                        <th scope="col">預計出貨日</th>
                        <th scope="col">預設班別</th>
                        <th scope="col">預設資源中心</th>
                        <th scope="col">APS製程碼</th>
                        <th scope="col">排單狀態</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
        <div style="text-align:right">
            <span style="display: inline-block; margin-top: 27px;">
                    <span>每頁顯示筆數</span>
                    <select id="bamount" onchange="getManufactureOrderData();$('#pagination-demo').twbsPagination('destroy');">
                        <option value="10" selected>10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                    </select>
            </span>
            <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
        </div>
        <hr>
    </div>
    <form action="{{ route('generate-simulation-scheme')}}" method="POST" id='form'>
        @csrf
        <input type="hidden" name="scheme_case" id="scheme_case" value="{{$scheme_case}}">
        <input type="hidden" value="{{$user}}" id="user" name="user">
        <div id="data_temp" hidden>        </div>
    </form>

    <div style="text-align:center">
        <button id="sendBtn" class="btn btn-success btn-lg" style="width:45%"  onclick="submit()" > 確認</button>
        <a class="btn btn-secondary btn-lg" href="{{ route('source-order') }}" style="width:45%">返回</a>
    </div>
            
</div>
<script>
    let normal = true;
    if($('#scheme_case').val() == 0){
        $('#assign_work').prop('disabled',true);
        $('#form').append('<input type="hidden" name="assign_work" value="1" />')
    } else {
        normal = false;
    }
    const clearCheck = () => {
        const e = $('input[name="mo_id[]"]');
        e.each(function(){
            this.checked = false;
        });
        $("#check_all").prop('checked',false);
    }
    const submit = () => {
        const e = $(':checked').length;

        console.log(e);
        if(e) $('#form').submit();
        else alert('請至少選擇一個項目');

    }
    //全選框事件處理
    const checkAll = (I) => {
      const e = $(':checkbox').filter('input[name^="mo_id"]');
      console.log()
      for (var i = e.length - 1; i >= 0; i--) {
        if(I.checked == true) e[i].checked = true ;
        else e[i].checked = false ;
        checkGroup(e[i],e[i].value);
      }

    }
    //單選框事件處理
    const checkGroup = (item,group) => {
      let e = $(`input[group='${group}']`);
      if(e[0].checked == true)
        $('#data_temp').append(`<div id="arr${group}"> </div>`);
      else $(`#data_temp > #arr${group}`).remove();
      for (var i=1;i<e.length;i++){
        e[i].checked = item.checked;
        $(`#data_temp > #arr${group}`).append($(e[i]).clone(true));
      }

    }
    const renderCheck = () => {
        let temp = $("#data_temp > div");
        for ( let i = 0;   i < temp.length;i++)
            $(`#box_${temp[i].id}`).prop('checked',true);
        $(`#check_all`).prop('checked', false);

    }
    let lastPage;
    const getManufactureOrderData = (page = 1) => {
        const amount = $('#amount').val();
        axios.get('{{ route('manufacture-order-result') }}' + location.search, {
            params: {
                amount,
                page,
            }
        }).then(({ data }) => {
            console.log(data);
            lastPage = data.last_page;
            const orders = data.data;
            $('#saleorder-data tbody').empty();
            const status_list = ['','已生效'] ;
            orders.forEach((order, key) => {
                key = (data.current_page-1)*data.per_page +key;
                let status = status_list[Number(order.status)] ;
                $('#saleorder-data tbody').append(`
                    <tr>
                        <td scope="row">
                            <input type="checkbox" name="mo_id[]" value="${key}" onclick="checkGroup(this,${key})" group='${key}' id="box_arr${key}">
                        <td>
                            <input type="checkbox" name="datas[${key}][mo_id]" value="${order.mo_id}"  group='${key}' hidden>${order.mo_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][item_id]" value="${order.item_id}"  group='${key}' hidden>${order.item_id}
                        </td>
                        <td>${order.so_id}</td>
                        <td>
                            <input type="checkbox" name="datas[${key}][qty]" value="${order.qty}"  group='${key}' hidden>${order.qty}
                        </td>
                        <td>${order.customer_name}</td>
                        <td>
                            <input type="checkbox" name="datas[${key}][cu_ush_date]" value="${order.cu_ush_date}"  group='${key}' hidden>${order.cu_ush_date}
                        </td>
                        <td>正常班</td>
                        <td>
                            <input type="checkbox" name="datas[${key}][resource_id]" value="${order.resource_id}"  group='${key}' hidden>${order.resource_id}
                        </td>
                        <td>
                            <input type="checkbox" name="datas[${key}][aps_id]" value="${order.aps_id}"  group='${key}' hidden>${order.aps_id}
                        </td>
                        <td>${status}</td>
                    </tr>
                `)
            })
            $('#pagination-demo').twbsPagination({
                totalPages: lastPage,
                visiblePages: 5,
                first:'頁首',
                last:'頁尾',
                prev:'<',
                next:'>',
                initiateStartPageClick: false,
                onPageClick: function (event, page) {
                    getManufactureOrderData(page);
                }
            });
        }).then(function(){
            renderCheck();
            $('#check_all').prop('checked',false);
        });
        let abc = 13;
        axios.get('{{route('work-type-data')}}',{
            params: {
                amount,
                page,
            }     
        })
          .then(res => {
            let shift = res.data.data;
            shift.forEach((item)=>{
                if(item.id == 1 && normal){
                    $('#assign_work').append(`<option value="${item.id}" selected>${item.name}</option>`);
                    return false;
                }
                if( item.id != 1 && !normal)
                    $('#assign_work').append(`<option value="${item.id}">${item.name}</option>`);
            });

          })


    }
    setTimeout(clearCheck ,100);
    getManufactureOrderData();
</script>
@endsection
