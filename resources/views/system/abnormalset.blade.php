@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .breadcrumb-custom {
        background-color: #3D404C;
        width: 99%;
        margin:0px auto;
        padding: 15px 15px;
        margin-bottom: 20px;
        list-style: none;
        border-radius: 4px;
        color: #fff;
    }
    .total-data {
        width: 98%;
        margin:0px auto;
        /* padding: 0px 10px; */
    }
    .table-pos {
        margin: 0px auto;
        width: 98%;
    }
    .thead-color {
        background-color: #E85726;
        color: #fff;
        height: 10px;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>異常原因設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">異常原因設定<span>
        </ol>
        <div class="breadcrumb-custom">
            <span>資料列表</span>
            <div style="float:right; margin-top:-7px">
                <a class="btn btn-success" href='{{route('abnormal-ctrl-add')}}'>新增</a>
            </div> 
        </div>
        <div class="total-data" id='amount'></div>
        <div style="margin-top:15px;">
            <table class="table table-striped table-pos">
                <thead class="thead-color">
                    <tr>
                        <th scope="col">序</th>
                        <th scope="col">異常類別</th>
                        <th scope="col">異常原因</th>
                        <th scope="col">操作</th>
                    </tr>
                </thead> 
                    <tbody id='table_body'>
                    </tbody>

            </table>
        </div>
            <div style="text-align:right">
                <span style="display: inline-block; margin-top: 27px;">
                        <span>每頁顯示筆數</span>
                        <select id="amount" onchange="getResData();$('#pagination-demo').twbsPagination('destroy');">
                            <option value="10" selected>10</option>
                            <option value="25">25</option>
                            <option value="50">50</option>
                        </select>
                </span>
                <ul id="pagination-demo" class="pagination-sm" style="vertical-align: top;"></ul>
            </div>
    </div>
</div>
<script>

    axios.post('{{route('abnormal-getData')}}')
    .then(({ data })=>{
        console.log(data);
        const amount = data.length;
        data.forEach((data,key) =>{
            $(table_body).append(`
                <tr>
                    <th scope="row">${key+1}</th>
                    <td>${data.Type}</td>
                    <td>${data.Exception}</td>
                    <td>
                        <a href ='{{url('abnormal-ctrl-fix/${data.no}')}}' name='${data.no}' class="btn btn-primary">編輯</a>
                        &nbsp
                        <a class="btn btn-danger" 
                            href='{{url('abnormal-del/${data.no}')}}'
                            >刪除</a>   
                    </td>
                </tr>


            `)

        })
        $('#amount').append(`載入筆數 | 共 ${amount} 筆`)
    });
</script>
@endsection