@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>資源設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">資源設定<span>
            <span class="space-item">></span>
            <span class="space-item">編輯資源設定</span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('resource.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">資源代碼</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="resource_id" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">資源名稱</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="resource_name" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">資源類別</label>
                                <div class="col-md-10">
                                    <select id="resource_type" name="resource_type" class="form-control" required>
                                        <option disabled selected value="">--- 請選擇資源類別 ---</option>
                                        <option value="設備" >設備</option>
                                        <option value="人力" >人力</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">設備編碼</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_id" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">機台名稱/規格</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_name" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">機台數量</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_qty" required>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">製程類別</label>
                                <div class="col-md-10">
                                    <select id="routing_select" name="routing_select" class="form-control" onchange="getCenter()" required>
                                        <option disabled selected value="">--- 請選擇班別類型 ---</option>
                                        <option value="11" >鐳射</option>
                                        <option value="12" >NCT</option>
                                        <option value="13" >P2</option>
                                        <option value="20" >沖床</option>
                                        <option value="30" >點焊</option>
                                        <option value="40" >塗裝</option>
                                        <option value="50" >裝配</option>
                                        <option value=""   >其它</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">工作中心代碼</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="workcenter_sel" name="workcenter_id" required>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">預設班別</label>
                                <div class="col-md-10">
                                    <select class="form-control" id="assign_work" name="assign_work" required>
                                    </select>                        
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準前置(小時)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_pre_time">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準TCT(秒)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_time">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準TCT</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="standard_tct">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">標準換線(分)</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="change_time">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">設備人力倍數</label>
                                <div class="col-md-10">
                                    <input type="text" class="form-control" name="device_multiple">
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">是否為預設資源中心</label>
                                <div class="col-md-10">
                                    <select id="is_default" name="is_default" class="form-control" required>
                                        <option value="0" >否</option>
                                        <option value="1" >是</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group" id="painting">
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%">確認</button>
                                <a class="btn btn-secondary btn-lg" href="{{ route('resource.index') }}" style="width:45%">返回</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>


    if ('{{ session('message') }}') {
        alert('{{ session('message') }}');
    }
    const getShift = () => {
        axios.get('{{route('get-work-time')}}')
        .then(({data}) => {
            console.log(data);
            $("#assign_work").append(`<option disabled selected value="">--- 請選擇 ---</option>`)
            data.forEach(data => {
                $("#assign_work").append(`<option value="${data.id}">${data.name}</option>`)
            })
        });
    }
    const getCenter = () => {
        axios.get('{{ route('getcenter') }}', {
            params: {
                value: $('#routing_select').val(),
            }
        })
        .then(({ data }) => {
            $("#workcenter_sel").empty();
            $("#workcenter_sel").append(`
                <option disabled selected value="">--- 請選擇 ---</option>
            `)
            data.forEach(data => {
                $("#workcenter_sel").append(`
                    <option value="${data.workcenter_id}">${data.workcenter_id}</option>
                `);
            })
        });
        if($('#routing_select').val()==40) {
            $('#painting').append(`
                <label class="col-md-2 control-label">烤漆線總長(公尺)</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="paint_length" required ">
                </div>
                <label class="col-md-2 control-label">總鉤數</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="hook_qty" required ">
                </div>
                <label class="col-md-2 control-label">換色時間(分)</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="change_color_time" required ">
                </div>
                <label class="col-md-2 control-label">標準換模時間(分)</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="standard_change_time" required ">
                </div>
                <label class="col-md-2 control-label">標準速率(D/T)M/min</label>
                <div class="col-md-10" style="margin-bottom: 15px;">
                    <input type="text" class="form-control" name="standard_rate" required >
                </div>
                <label class="col-md-2 control-label">完工空勾距離</label>
                <div class="col-md-10">
                    <input type="text" class="form-control" name="empty_hook_distance" required >
                </div>
            `);
        }
        else {
            $('#painting').empty();
        }
    }
    getShift();

</script>

@endsection
