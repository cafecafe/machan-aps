@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>角色設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">角色設定<span>
            <span class="space-item">></span>
            <span class="space-item">編輯角色設定</span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('role-set.update', $data->id) }}" method="POST">
                            @csrf
                            @method("PUT")
                            <div class="form-group">
                                <label class="col-md-2 control-label">組織別</label>
                                <div class="col-md-10">
                                    <select id="org_select" name="org_select" class="form-control" required>
                                        <option value="1000" {{ $data->factory_id === '1000' ? 'selected' : '' }} >一群</option>
                                        <option value="1002" {{ $data->factory_id === '1002' ? 'selected' : '' }} >二群</option>
                                        <option value="1003" {{ $data->factory_id === '1003' ? 'selected' : '' }} >三群</option>
                                        <option value="1009" {{ $data->factory_id === '1009' ? 'selected' : '' }} >五群</option>
                                        <option value="1005" {{ $data->factory_id === '1005' ? 'selected' : '' }} >六群</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">製程類別</label>
                                <div class="col-md-10">
                                    <select id="routing_select" name="routing_select" class="form-control" required>
                                        <option value="11" {{ $data->routing_level === '11' ? 'selected' : '' }}>鐳射</option>
                                        <option value="12" {{ $data->routing_level === '12' ? 'selected' : '' }}>NCT</option>
                                        <option value="13" {{ $data->routing_level === '13' ? 'selected' : '' }}>P2</option>
                                        <option value="20" {{ $data->routing_level === '20' ? 'selected' : '' }}>沖床</option>
                                        <option value="30" {{ $data->routing_level === '30' ? 'selected' : '' }}>點焊</option>
                                        <option value="40" {{ $data->routing_level === '40' ? 'selected' : '' }}>塗裝</option>
                                        <option value="50" {{ $data->routing_level === '50' ? 'selected' : '' }}>裝配</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <input type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%" value="確認">
                                <a class="btn btn-secondary btn-lg" href="{{ route('role-set.index') }}" style="width:45%">返回</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
