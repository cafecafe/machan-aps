@extends('layouts.myapp')

@section('css')
<style>
    .space-item {
        margin-left: 10px;
    }
    .panel-default {
        border-color: #000000;
    }
    .panel-default > .panel-heading {
        color: #fff;
        background-color: #000000;
        border-color: #000000;
    }
    .form-horizontal .control-label {
        text-align: center;
    }
    hr {
        border-top: 1px solid #ccc;
    }
    .btn-secondary {
        color: #fff;
        background-color: #6c757d;
        border-color: #6c757d;
    }
    .btn-secondary:hover {
        color: #fff;
        background-color: #5a6268;
        border-color: #545b62;
    }
    .btn.focus, .btn:focus, .btn:hover {
        color: #fff;
    }
</style>
@endsection

@section('content')
<div id="page-wrapper">
    <div class="container-fluid">
        <h2>角色設定</h2>
        <ol class="breadcrumb">
            <img src="{{ asset('img/u12.png') }}">
            <span class="space-item">系統管理</span>
            <span class="space-item">></span>
            <span class="space-item">角色設定<span>
            <span class="space-item">></span>
            <span class="space-item">編輯角色設定</span>
        </ol>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">資料編輯</div>
                    <div class="panel-body">
                        <form class="form-horizontal" action="{{ route('role-set.store') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-2 control-label">組織別</label>
                                <div class="col-md-10">
                                    <select id="org_select" name="org_select" class="form-control" required>
                                        <option disabled selected value="">--- 請選擇組織別---</option>
                                        <option value="1000" >一群</option>
                                        <option value="1002" >二群</option>
                                        <option value="1003" >三群</option>
                                        <option value="1009" >五群</option>
                                        <option value="1005" >六群</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-md-2 control-label">製程類別</label>
                                <div class="col-md-10">
                                    <select id="routing_select" name="routing_select" class="form-control" required>
                                        <option disabled selected value="">--- 請選擇班別類型 ---</option>
                                        <option value="11" >鐳射</option>
                                        <option value="12" >NCT</option>
                                        <option value="13" >P2</option>
                                        <option value="14" >捲料</option>
                                        <option value="20" >沖床</option>
                                        <option value="30" >點焊</option>
                                        <option value="40" >塗裝</option>
                                        <option value="50" >裝配</option>
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div style="text-align:center">
                                <button type="submit" id="sendBtn" class="btn btn-success btn-lg" style="width:45%">確認</button>
                                <a class="btn btn-secondary btn-lg" href="{{ route('role-set.index') }}" style="width:45%">返回</a>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
